/**
 * Created by vanbrabantwesley on 25/12/14.
 */

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concatCss = require('gulp-concat-css');
var clean = require('gulp-rimraf');
var cssMin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var jsValidate = require('gulp-jsvalidate');
var localScreenshots = require('gulp-local-screenshots')


var moveFiles =
    [
        'App/404.html',
        'App/components',
        'App/content',
        'App/index.html',
        'App/Styles',
        'App/Scripts'
    ];

var subDir1 =
    [
        'App/components/**/*'
    ];

var subDir2 =
    [
        'App/content/**/*'
    ];

gulp.task('clean', function(){
    return gulp.src('Dist/*', {read:false})
        .pipe(clean());
});


gulp.task('move',['clean'], function(){
    // the base option sets the relative root for the set of files,
    // preserving the folder structure
    gulp.src(moveFiles, { base: '' })
        .pipe(gulp.dest('dist'));

    gulp.src(subDir1, {base: ''})
        .pipe(gulp.dest('dist/components'));

    gulp.src(subDir2, {base: ''})
        .pipe(gulp.dest('dist/content'));
});


gulp.task('concat', ['move'], function(){
    gulp.src('App/styles/*.css')
        .pipe(concatCss("styles.css"))
        .pipe(gulp.dest('App/Styles'));

    gulp.src('App/styles/styles.css')
        .pipe(cssMin())
        .pipe(rename({basename:'app',suffix: '.min'}))
        .pipe(gulp.dest('dist/Styles'));

});


gulp.task('compress',['concat'], function(){
    gulp.src('App/Scripts/*.js')
        .pipe(concat('script.js'))
        .pipe(gulp.dest('App/Scripts'));

    gulp.src('App/Scripts/script.js')
        .pipe(uglify())
        .pipe(rename({basename:'app',suffix: '.min'}))
        .pipe(gulp.dest('dist/Scripts'));
});

gulp.task('validate', ['compress'], function () {
    return gulp.src('dist/scripts/app.min.js')
        .pipe(jsValidate());

});

gulp.task('screens', ['validate'], function () {
    gulp.src('App/index.html')
        .pipe(localScreenshots({
            width: ['1200', '992','768' , '480', '320'],
            type:'jpg',
            height:'577',
            folder:'dist/breakpointScreenshots'
        }))
        .pipe(gulp.dest('dist/breakpointScreenshots/'));
});



gulp.task('default',['screens'], function() {



});





