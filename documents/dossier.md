1. Analyse & Concept
--------------------
###1.1 Briefing
Een To-do web-applicatie maken. Deze helpt mensen bij het onthouden van taken of opdrachten die nog moeten worden verricht. We hebben een grote doelgroep voor ogen en functionaliteiten om mensen aan te sporen er vaak gebruik van te maken (loop: to-do taak om to-do app te gebruiken).

###1.2 Doelgroepen
De doelgroep voor de webapp 'Have a cookie' is heel ruim. Elke smartphone gebruiker met een beetje kennis kan er mee over weg. Dus elke persoon tussen de 12 en 70 jaar zit in ons doelpubliek.
###1.3 Analyse
We gaan een web-applictie maken die zowel goed werkt voor mobiel als desktop. Zo kan de gebruiker zijn to-do lijst overal raadplegen.
###1.4 Specificaties
####1.4.1 Technische specificaties
HTML5
CSS3
Javascript
Jquery
JSON
LocalStorage
context.JS
simptip.JS


####1.4.2 Functionele specificaties
#####1.4.2.1 Verplichte functionaliteit
Lijsten beheren
Categorieën beheren
Taken toekennen aan een lijst
Taken voltooien
Voltooing ongedaan maken
Kleurcodes (+ prioriteit)
Due-dates beheren
Reminders
#####1.4.2.2 Optionele functionaliteit
Welcome tutorial
Notificaties
Exporteren
Omschrijvingen
Custom context menu
Challenges
Beloning (get cookie for task)
Sorteren van taken
Lijst met prioriteiten
Undo action / geschiedenis

2. User Experience
---------------
###2.1 Persona Sheets
####2.1.1 Gebruikers
Naam: Hugo
Leeftijd: 44 jaar
Burgerlijke staat: Getrouwd
Kinderen: 2
Devices: iMac, iPhone
Browser: Safari
Gebruik WebApp: Hugo gebruikt de webapp om niets te vergeten van zijn huishoudelijke taken.
User-Scenario:
        hugo ontdekt de webapp via een vriend op Facebook.
        Hij neemt een kijkje op de webapp. Hugo vindt dat de webapp een leuke layout heeft.
        Hugo besluit enkele herinneringen toe te voegen.
        Hugo verlaat de webapp.
Ervaring webapp:
        Hugo vindt de applicatie overzichtelijk en gemakkelijk in gebruik.

---
Naam: Nicole
Leeftijd: 30 jaar
Burgerlijke staat: Alleenstaand
Kinderen: 0
Devices: PC, Samsung Galaxy S3
Browser: Chrome
Gebruik WebApp: Nicole gebruikt de webapp voor haar boodschappenlijstje en afspraken dat ze niet mag vergeten.
User-Scenario:
        Nicole is op zoek naar een reminder-app op google.
        Ze komt terecht op de webapp 'Have a cookie'.
        Nicole vindt de webapp leuk en maakt meteen een boodschappenlijstje op.
        Nicole verlaat de webapp en gaat winkelen.
Ervaring webapp:
        Nicole vindt het handig dat ze de webapp zowel mobiel als op haar desktop kan gebruiken.

---
Naam: Bart
Leeftijd: 19 jaar
Burgerlijke staat: Alleenstaand
Kinderen: 0
Devices: PC, HTC one, Samsung tablet
Browser: Chrome en Firefox
Gebruik WebApp: Bart gebruikt de webapp voor zijn schooltaken bij te houden.
User-Scenario:
	Bart zag een advertentie staan op internet over een nieuwe webapp en besloot hem uit te 			proberen.
	Bart vindt de webapp er goed uitzien en maakt enkele lijstjes met schoolvakken.
	Hij geeft al zijn opdrachten in voor deze week en besluit de webapp zeker te gebruiken.
	Bart verlaat de webapp.
Ervaring webapp:
       Bart heeft overzicht over zijn taken en kan zo beter plannen.
3. Information Architecture
-------
###3.1 Sitemap
![Sitemap](https://lh4.googleusercontent.com/-Pq8hKrad0xE/VJ2fgDuaovI/AAAAAAAAAPE/No3a8-sKl_c/s0/Sitemap.png "Sitemap.png")

###3.2 Wireframes/Wireflow
![Wireframe/Wireflow](https://lh3.googleusercontent.com/-jLkVTt92t0o/VJ2nTnTrlSI/AAAAAAAAAPY/N7JC0m5_Gng/s0/Wireframes2.png "Wireframes2.png")

###3.3 Visual Design
####3.2.1 Moodboard
![Moodboard](https://lh3.googleusercontent.com/-Vpx96wfJ9_U/VJ2o8EclqII/AAAAAAAAAPo/mL24nHA7zgU/s0/moodboard.png "moodboard.png")

####3.2.2 Style Tile

![Styletile](https://lh5.googleusercontent.com/-HZi563BLIiM/VJ2plX3WU5I/AAAAAAAAAP0/sbzJhQJJeM8/s0/Styletile.png "Styletile.png")

![Styletile2](https://lh4.googleusercontent.com/-DWUe9sZZu8A/VJ2p71k78cI/AAAAAAAAAQA/jcyGsjlj61Q/s0/Styletile2.png "Styletile2.png")

####3.3.3 Designs
![Design](https://lh6.googleusercontent.com/-UiedW4qyRvU/VJ2qt_NNSBI/AAAAAAAAAQU/ZONsyVlAmZo/s0/Design4.png "Design4.png")

4. Poster
-----------
![Poster](https://lh5.googleusercontent.com/-SiP47e5l1nY/VJ2q_zyk0PI/AAAAAAAAAQk/j25N6rD9BB4/s0/Poster6.jpg "Poster6.jpg")
