##Remember The Cocoa assignment
---
### Tabel

1. Team For | Group 11
	1.1 Team Members 
=======
1. Team 'Have a Cookie' | Group 11
2. Briefing
3. Analyse
4. Technische specificaties
5. Functionele specificaties
6. Persona's (+ scenario)
7. Moodboard
8. Sitemap
9. Wireframes

---
###Team Spacedog | Group 11
####Team members
* Wesley Vanbrabant
* Lorenz De Cuypere
* Maxim Vanhove

### Briefing
Een To-do web-applicatie maken. Deze helpt mensen bij het onthouden van taken of 
opdrachten die nog moeten worden verricht.
We hebben een grote doelgroep voor ogen en functionaliteiten om mensen aan
te sporen er vaak gebruik van te maken (loop: to-do taak om to-do app te gebruiken).

---
### Analyse

We gaan een web-applictie maken die zowel goed werkt voor mobiel als desktop. Zo kan de gebruiker zijn to-do lijst overal raadplegen.


####Doelgroep
De doelgroep voor de webapp 'Have a cookie' is heel ruim. Elke smartphone gebruiker met een beetje kennis kan er mee over weg.
Dus elke persoon tussen de 12 en 70 jaar zit in ons doelpubliek.

----

### Technische specificaties 
* HTML5
* CSS3
* Javascript
* Jquery
* JSON
* LocalStorage
* context.JS
* simptip.JS

----

### functionele specificaties

#### Verplichte functionaliteit

* Lijsten beheren
* categorieën beheren
* taken toekennen aan een lijst
* taken voltooien
* voltooing ongedaan maken
* kleurcodes (+ prioriteit)
* due-dates beheren
* reminders


#### Optionele functionaliteit
 
* welcome tutorial
* notificaties
* exporteren
* omschrijvingen
* custom context menu
* challenges
* beloning (get cookie for task)
* sorteren van taken
* lijst met prioriteiten
* undo action / geschiedenis

---

### persona's (+ scenario)


####Gebruiker 1


* Naam: Hugo
* Leeftijd: 44 jaar
* Burgerlijke staat: Getrouwd
* Kinderen: 2
*  Devices: iMac, iPhone
* Browser: Safari
*  Gebruik WebApp: Hugo gebruikt de webapp om niets te vergeten van zijn huishoudelijke taken.

* User-Scenario:
	* hugo ontdekt de webapp via een vriend op Facebook.
	* Hij neemt een kijkje op de webapp. Hugo vindt dat de webapp een leuke layout heeft.
	* Hugo besluit enkele herinneringen toe te voegen.
	* Hugo verlaat de webapp.
	
* Ervaring webapp:
    * Hugo vindt de applicatie overzichtelijk en gemakkelijk in gebruik.
***

####Gebruiker 2

* Naam: Nicole
* Leeftijd: 30 jaar
* Burgerlijke staat: Alleenstaand
* Kinderen: 0
*  Devices: PC, Samsung Galaxy S3
* Browser: Chrome
*  Gebruik WebApp: Nicole gebruikt de webapp voor haar boodschappenlijstje en afspraken dat ze niet mag vergeten.

* User-Scenario:
	* Nicole is op zoek naar een reminder-app op google.
	* Ze komt terecht op de webapp 'Have a cookie'.
	* Nicole vindt de webapp leuk en maakt meteen een boodschappenlijstje op.
	* Nicole verlaat de webapp en gaat winkelen.

* Ervaring webapp:
    * Nicole vindt het handig dat ze de webapp zowel mobiel als op haar desktop kan gebruiken.


---

### moodboard

[Moodboard deel 1](http://nl.pinterest.com/vanbrabantwesle/to-do-web-application-moodboard/) Deel 1 - pinterest

[Moodboard deel 2](http://nl.pinterest.com/lorenzdecuypere/todo-app/) Deel 2 - pinterest

[Moodboard deel 3](https://color.adobe.com/nl/Shades-of-grey-an-umph-color-theme-65755/) Deel 3 - Adobe Kuler Color Scheme


---

### sitemap

[sitemap](https://bitbucket.org/MaximVanhove/nmdadi_201415_cocoa_2mmpa_groep11/src/5871868764e310bba0ff1404a0b32613a48c8e42/Milestone1/Sitemap.png?at=master)

---

### wireframes
[wireframes](https://bitbucket.org/MaximVanhove/nmdadi_201415_cocoa_2mmpa_groep11/src/5871868764e310bba0ff1404a0b32613a48c8e42/Milestone1/Wireframes.png?at=master)

