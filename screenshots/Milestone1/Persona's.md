Persona Sheets
-------------------------

####Gebruiker 1


* Naam: Frankie van Zwet
* Leeftijd: 44 jaar
* Burgerlijke staat: Getrouwd
* Kinderen: 2
*  Devices: iMac, iPhone
* Browser: Safari
*  Gebruik WebApp: Frankie gebruikt de webapp om niets te vergeten van zijn huishoudelijke taken.

* User-Scenario:
	* Frankie ontdekt de webapp via een vriend op Facebook.
	* Hij neemt een kijkje op de webapp. Frankie vindt dat de webapp een leuke layout heeft.
	*  Frankie besluit enkele herinneringen toe te voegen.
	* Frankie verlaat de webapp.

* Ervaring webapp:
    * Frankie vindt de applicatie overzichtelijk en gemakkelijk in gebruik.
***

####Gebruiker 2

* Naam: Lies Vercruyse
* Leeftijd: 30 jaar
* Burgerlijke staat: Alleenstaand
* Kinderen: 0
*  Devices: PC, Samsung Galaxy S3
* Browser: Chrome
*  Gebruik WebApp: Lies gebruikt de webapp voor haar boodschappenlijstje en afspraken dat ze niet mag vergeten.

* User-Scenario:
	* Lies is op zoek naar een reminder-app op google.
	* Ze komt terecht op de webapp 'Have a cookie'.
	* Lies vindt de webapp leuk en maakt meteen een boodschappenlijstje op.
	* Lies verlaat de webapp en gaat winkelen met haar smartphone.

* Ervaring webapp:
    * Lies vindt het handig dat ze de webapp zowel mobiel als op haar desktop kan gebruiken.
***

####Gebruiker 3
	
Naam: Bart
Leeftijd: 19 jaar
Burgerlijke staat: Alleenstaand
Kinderen: 0
Devices: PC, HTC one, Samsung tablet
Browser: Chrome en Firefox
Gebruik WebApp: Bart gebruikt de webapp voor zijn schooltaken bij te houden. 
User-Scenario:
	Bart zag een advertentie staan op internet over een nieuwe webapp en besloot hem uit te 			proberen.
	Bart vindt de webapp er goed uitzien en maakt enkele lijstjes met schoolvakken.
	Hij geeft al zijn opdrachten in voor deze week en besluit de webapp zeker te gebruiken.
	Bart verlaat de webapp.
Ervaring webapp:
       Bart heeft overzicht over zijn taken en kan zo beter plannen.
